#pragma once

#include <typedefs.h>

#include "./triangle_mesh.h"

#include "../../maths/maths.h"

namespace RED
{
    namespace assets
    {
        namespace triangle
        {
            namespace cube
            {
                //https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_05
                math::v3 cube_vertices[] = {
                    // front
                    {-1.0, -1.0,  1.0},
                    {1.0, -1.0,  1.0},
                    {1.0,  1.0,  1.0},
                    {-1.0,  1.0,  1.0},
                    // back
                    {-1.0, -1.0, -1.0},
                    {1.0, -1.0, -1.0},
                    {1.0,  1.0, -1.0},
                    {-1.0,  1.0, -1.0}
                };

                u32 cube_elements[] = {
                    // front
                    0, 1, 2,
                    2, 3, 0,
                    // right
                    1, 5, 6,
                    6, 2, 1,
                    // back
                    7, 6, 5,
                    5, 4, 7,
                    // left
                    4, 0, 3,
                    3, 7, 4,
                    // bottom
                    4, 5, 1,
                    1, 0, 4,
                    // top
                    3, 2, 6,
                    6, 7, 3
                };

                TriangleMesh loadCubeTriangleMesh(u32 id)
                {
                    TriangleMesh mesh{};
                    mesh.id = id;

                    mesh.amt_verts = 8;
                    mesh.vert_positions = cube_vertices;

                    mesh.amt_indices = 36;
                    mesh.indices = cube_elements;

                    return mesh;
                }
            }
        }
    }
}