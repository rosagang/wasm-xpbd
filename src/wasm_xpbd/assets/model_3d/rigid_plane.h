#pragma once

#include <typedefs.h>

#include "./triangle_mesh.h"

#include "../../maths/maths.h"

namespace RED
{
    namespace assets
    {
        namespace triangle
        {
            namespace plane
            {
                math::v3 plane_vertices[] = {
                    {-1.0, 0.,  -1.0},
                    {1.0, 0.,  -1.0},
                    {-1.0,  0.,  1.0},
                    {1.0,  0.,  1.0},
                };

                u32 plane_elements[] = {
                    // front
                    0, 1, 2,
                    2, 3, 1,                   
                };

                TriangleMesh loadPlaneTriangleMesh(u32 id)
                {
                    TriangleMesh mesh{};
                    mesh.id = id;

                    mesh.amt_verts = 4;
                    mesh.vert_positions = plane_vertices;

                    mesh.amt_indices = 6;
                    mesh.indices = plane_elements;

                    return mesh;
                }
            }
        }
    }
}