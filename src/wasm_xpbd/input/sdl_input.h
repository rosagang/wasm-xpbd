#pragma once

#include <sdl2/SDL.h>

#include <typedefs.h>

#include "input.h"
#include "maths/maths.h"

namespace RED
{
    namespace input
    {
        const i32 MAX_NUM_KEYS = 512;

        struct SDL_Input
        {
            math::v2 mouseLastFrame;  
            b32 is_grabbing;
            u8 prev_key_states[MAX_NUM_KEYS];
        };

        SDL_Input setup()
        {
            i32 x;
            i32 y;
            u32 mouse_state = SDL_GetMouseState(&x, &y);
            SDL_Input sdl_input{};
            
            sdl_input.mouseLastFrame = {(f32) x, (f32) y};
            i32 numkeys;
            const u8* key_state = SDL_GetKeyboardState(&numkeys);
            memcpy(sdl_input.prev_key_states, key_state, std::min(numkeys, MAX_NUM_KEYS));

            return sdl_input;
        }

        Input generateFromSDL(SDL_Input *sdl_input)
        {
            Input input = {};

            i32 numkeys;
            const u8* key_state = SDL_GetKeyboardState(&numkeys);

            if(key_state[SDL_SCANCODE_W])
            {
                input.moveCmds[(u32) MOVE_DIR::FORWARD] = {1.};   
            }
            if(key_state[SDL_SCANCODE_S])
            {
                input.moveCmds[(u32) MOVE_DIR::BACKWARD] = {1.};   
            }
            if(key_state[SDL_SCANCODE_A])
            {
                input.moveCmds[(u32) MOVE_DIR::LEFT] = {1.};   
            }
            if(key_state[SDL_SCANCODE_D])
            {
                input.moveCmds[(u32) MOVE_DIR::RIGHT] = {1.};   
            }
           
            if(key_state[SDL_SCANCODE_ESCAPE])
            {
                input.stop_running = true;   
            }

            
            if(key_state[SDL_SCANCODE_E] && !sdl_input->prev_key_states[SDL_SCANCODE_E])
            {
                input.step_once = true;  
            }
            if(key_state[SDL_SCANCODE_Q] && !sdl_input->prev_key_states[SDL_SCANCODE_Q])
            {
                input.step = true;  
            }
            if(key_state[SDL_SCANCODE_G] && !sdl_input->prev_key_states[SDL_SCANCODE_G])
            {
                input.toggle_grav = true;  
            }


            memcpy(sdl_input->prev_key_states, key_state, std::min(numkeys, MAX_NUM_KEYS));


            i32 x;
            i32 y;
            u32 mouse_state = SDL_GetMouseState(&x, &y);
            math::v2 mouse_this_frame{ (f32) x, (f32) y };           

            if(mouse_state & SDL_BUTTON_RMASK)
            {                
                math::v2 dmouse = mouse_this_frame - sdl_input->mouseLastFrame;
                input.rotCameraCmds = { dmouse };
            }

            sdl_input->mouseLastFrame = mouse_this_frame;


            input.grab_command.xy_screen = mouse_this_frame; 

            if(mouse_state & SDL_BUTTON_LMASK)
            {               
                if(sdl_input->is_grabbing)
                {
                    input.grab_command.action = GRAB_ACTION::DRAG;
                }
                else
                {
                    input.grab_command.action = GRAB_ACTION::START;
                    sdl_input->is_grabbing = true;
                }
            }            
            else
            {
                if(sdl_input->is_grabbing)
                {
                    input.grab_command.action = GRAB_ACTION::END;
                    sdl_input->is_grabbing = false;
                }
                else
                {
                    input.grab_command.action = GRAB_ACTION::NOTHING;
                }
            }

            SDL_Event event = {};
            while (SDL_PollEvent(&event))
            {                
                if (event.type == SDL_QUIT)
                {
                    input.stop_running = true;
                }
            }

            return input;
        }
    }
}