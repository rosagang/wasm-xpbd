#pragma once

#include <vector>

#include "../xpbd/phyiscs_bodies/soft_body.h"
#include "../xpbd/phyiscs_bodies/rigid_body.h"

namespace RED
{
    namespace render
    {
        namespace scene
        {
            struct Line
            {
                math::v3 color;
                math::v3 p1;
                math::v3 p2;
            };

            struct Scene
            {
                std::vector<sim::SoftBody *> soft_bodies_to_render_ptr;
                std::vector<sim::RigidBody *> rigid_bodies_to_render_ptr;

                std::vector<Line> lines;
                std::vector<Line> debug_lines;

                std::vector<math::Rect> rects;
                std::vector<math::Rect> debug_rects;
                std::vector<math::Point> debug_points;
                std::vector<math::Triangle> debug_triangles;
                
                math::v3 point_light_1;
            };

            void pushSoftBody(Scene *scene, sim::SoftBody *soft_body)
            {
                scene->soft_bodies_to_render_ptr.push_back(soft_body);
            }

            void pushRigidBody(Scene *scene, sim::RigidBody *rigid_body)
            {
                scene->rigid_bodies_to_render_ptr.push_back(rigid_body);
            }

            void pushRect(Scene *scene, math::Rect rect)
            {
                scene->rects.push_back(rect);
            }

            void pushLine(Scene *scene, Line line)
            {
                scene->lines.push_back(line);
            }

            void pushRectAroundPoint(Scene *scene, math::v3 c, math::v3 p)
            {
                scene->rects.push_back({
                    c,
                    p + math::v3(.1),
                    p - math::v3(.1)
                });
            }

           
        }
    }
}