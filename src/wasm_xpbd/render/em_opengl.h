#pragma once

#include <emscripten.h>
#include <emscripten/html5.h>
#include <GLES3/gl32.h>

#include "opengl.h"
#include "scene.h"

#include "../xpbd/sim.h"


namespace RED
{
    namespace render
    {
        const char * canvasId = "canvas";

        struct EM_Opengl
        {
            f32 canvas_left;
            f32 canvas_top;
            cam::Camera *camera_ptr;
        };

        EM_Opengl em_opengl{};

        void handleCanvasResize()
        {
            i32 w;
            i32 h;
            emscripten_get_canvas_element_size(canvasId, &w, &h);
            if(w != em_opengl.camera_ptr->w || h != em_opengl.camera_ptr->h)
            {
                cam::setupCamera(
                    em_opengl.camera_ptr, 
                    em_opengl.camera_ptr->pos, 
                    em_opengl.camera_ptr->view_dir, 
                    w, h, 
                    em_opengl.camera_ptr->fov); 

                em_opengl.canvas_left = EM_ASM_DOUBLE_V(
                    {
                        const rect = document.getElementById("canvas").getBoundingClientRect();
                        return rect.left;
                    }
                );

                em_opengl.canvas_top = EM_ASM_DOUBLE_V(
                    {
                        const rect = document.getElementById("canvas").getBoundingClientRect();
                        return rect.top;
                    }
                );          

                glViewport(0, 0, w, h);
            }
        }

        void make_canvas_bigger()
        {
            i32 w;
            i32 h;
            emscripten_get_canvas_element_size(canvasId, &w, &h);
            emscripten_set_canvas_element_size(canvasId, w * 1.1, h * 1.1);
            handleCanvasResize();
        }

        void make_canvas_smaller()
        {
            i32 w;
            i32 h;
            emscripten_get_canvas_element_size(canvasId, &w, &h);
            emscripten_set_canvas_element_size(canvasId, w * .9, h * .9);
            handleCanvasResize();            
        }

        void initOpenGL(cam::Camera *cam)
        {
            EmscriptenWebGLContextAttributes attr;
            emscripten_webgl_init_context_attributes(&attr);
            attr.alpha = false;
            attr.depth = true;
            attr.enableExtensionsByDefault = 1;
            attr.majorVersion = 2;
            attr.minorVersion = 0;
            EMSCRIPTEN_WEBGL_CONTEXT_HANDLE ctx = emscripten_webgl_create_context(canvasId, &attr);

            emscripten_webgl_make_context_current(ctx);

            glEnable(GL_DEPTH_TEST);

            em_opengl.camera_ptr = cam;            

            emscripten_set_canvas_element_size(canvasId, 1920 * .5, 1080 * .5);

            handleCanvasResize();

            ogl3::setupAfterCtxCreation(em_opengl.camera_ptr->w, em_opengl.camera_ptr->h);
        }

        math::v2 clientXYToCanvasXY(math::v2 clientXY)
        {
            math::v2 res{};

            res.x = clientXY.x - em_opengl.canvas_left;
            res.y = clientXY.y - em_opengl.canvas_top;

            return res;
        }

        void renderSceneAndSwapWindows(scene::Scene *scene, cam::Camera *cam)
        {
            handleCanvasResize();
            ogl3::drawSceneToBackbuffer(scene, cam);
        }
    }
}