#pragma once

#include <typedefs.h>
#include <unordered_map>

#include "./util.h"
#include "./program.h"
#include "./camera.h"
#include "./scene.h"


#include "./shader/soft_body_shading.h"
#include "./shader/rigid_body_shading.h"
#include "./shader/draw_points.h"

namespace RED
{
    namespace render
    {
        namespace ogl3
        {      
            struct Programs
            {
                Program soft_body_shading;
                Program rigid_body_shading;
                Program draw_points;
            };

            Programs programs;               

            void setupAfterCtxCreation(f32 w, f32 h)
            {
                
                glClearColor(0.3, 0.4, 0.3, 1.);                
                glLineWidth(3);
                //glPointSize(5);

                glEnable(GL_BLEND);  
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  
                
                programs.soft_body_shading = util::loadShadersAndLinkProgramFromCode(
                    {
                        soft_body_shading::VERT,
                        soft_body_shading::FRAG
                    },
                    {
                        GL_VERTEX_SHADER,
                        GL_FRAGMENT_SHADER
                    }
                );

                programs.rigid_body_shading = util::loadShadersAndLinkProgramFromCode(
                    {
                        rigid_body_shading::VERT,
                        rigid_body_shading::FRAG
                    },
                    {
                        GL_VERTEX_SHADER,
                        GL_FRAGMENT_SHADER
                    }
                );

                programs.draw_points = util::loadShadersAndLinkProgramFromCode(
                    {
                        draw_points::VERT,
                        draw_points::FRAG
                    },
                    {
                        GL_VERTEX_SHADER,
                        GL_FRAGMENT_SHADER
                    }
                );
            }

            struct GPU_Memory
            {
                std::unordered_map<u32, u32> teth_mesh_ids_to_index_buffer;
                std::unordered_map<u32, u32> triangle_mesh_ids_to_index_buffer;
                std::unordered_map<u32, u32> triangle_mesh_ids_to_vertex_buffer;
                std::unordered_map<u32, u32> triangle_mesh_ids_to_normal_buffer;
                std::unordered_map<u32, u32> triangle_mesh_ids_to_vao;          
            };

            GPU_Memory gpu_memory;

            u32 calculateNormalsFromPositionsPushOnGPUReturnBuffer(u32 amt_indices, u32 *indices, u32 amt_vertices, math::v3 *positions)
            {
                u32 size_in_bytes = amt_vertices * sizeof(math::v3);
                math::v3 *normals = (math::v3 *) malloc(size_in_bytes);
                memset(normals, 0, size_in_bytes);

                for(u32 i = 0; i < amt_indices; i += 3)
                {
                    u32 i1 = indices[i];
                    u32 i2 = indices[i + 1];
                    u32 i3 = indices[i + 2];
                
                    math::v3 p1 = positions[i1]; 
                    math::v3 p2 = positions[i2];
                    math::v3 p3 = positions[i3];

                    math::v3 p1p2 = p2 - p1;
                    math::v3 p1p3 = p3 - p1;

                    math::v3 face_normal = -glm::cross(p1p3, p1p2);

                    normals[i1] += face_normal;
                    normals[i2] += face_normal;
                    normals[i3] += face_normal;
                }

                u32 buffer;
                glGenBuffers(1, &buffer);
                glBindBuffer(GL_ARRAY_BUFFER, buffer);

                glBufferData(GL_ARRAY_BUFFER, size_in_bytes, normals, GL_DYNAMIC_DRAW);
                glEnableVertexAttribArray((u32) SHADER_ATTR::A_NORMAL);
                glVertexAttribPointer((u32) SHADER_ATTR::A_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

                return buffer;
            }   

            void tethMeshAdded(assets::teth::TethMesh *teth_mesh)
            {
                u32 buffer;
                glGenBuffers(1, &buffer);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);

                u32 size_in_bytes = teth_mesh->indices_for_rendering.size() * sizeof(u32);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, size_in_bytes, teth_mesh->indices_for_rendering.data(), GL_STATIC_DRAW);

                gpu_memory.teth_mesh_ids_to_index_buffer.insert({teth_mesh->id, buffer});
            }            

            void triMeshAdded(assets::triangle::TriangleMesh *tri_mesh)
            {
                u32 vao;
                u32 ubo;
                u32 ebo;
                glGenVertexArrays(1, &vao);
                glBindVertexArray(vao);

                glGenBuffers(1, &ubo);
                glBindBuffer(GL_ARRAY_BUFFER, ubo);
                glBufferData(GL_ARRAY_BUFFER, tri_mesh->amt_verts * sizeof(math::v3), tri_mesh->vert_positions, GL_STATIC_DRAW);
                glEnableVertexAttribArray((u32) SHADER_ATTR::A_POS);
                glVertexAttribPointer((u32) SHADER_ATTR::A_POS, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

                u32 normal_buffer = calculateNormalsFromPositionsPushOnGPUReturnBuffer(
                    tri_mesh->amt_indices, 
                    tri_mesh->indices, 
                    tri_mesh->amt_verts, 
                    tri_mesh->vert_positions);

                glGenBuffers(1, &ebo);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, tri_mesh->amt_indices * sizeof(u32), tri_mesh->indices, GL_STATIC_DRAW);
               
                glBindVertexArray(0);
                glBindBuffer(GL_ARRAY_BUFFER, 0);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

                u32 id = tri_mesh->id;
                gpu_memory.triangle_mesh_ids_to_index_buffer.insert({id, ebo});
                gpu_memory.triangle_mesh_ids_to_vertex_buffer.insert({id, ubo});
                gpu_memory.triangle_mesh_ids_to_normal_buffer.insert({id, normal_buffer});
                gpu_memory.triangle_mesh_ids_to_vao.insert({id, vao});
            }

            u32 pushVerticesOntoGPUReturnBuffer(math::v3 *verts, u32 amt)
            {
                u32 buffer;
                glGenBuffers(1, &buffer);
                glBindBuffer(GL_ARRAY_BUFFER, buffer);

                u32 size_in_bytes = amt * sizeof(math::v3);
                glBufferData(GL_ARRAY_BUFFER, size_in_bytes, verts, GL_DYNAMIC_DRAW);

                glEnableVertexAttribArray((u32) SHADER_ATTR::A_POS);
                glVertexAttribPointer((u32) SHADER_ATTR::A_POS, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

                return buffer;
            }

            u32 calculateSoftBodyNormalVectorsPushOnGPUReturnBuffer(sim::SoftBody *soft_body)
            {
                return calculateNormalsFromPositionsPushOnGPUReturnBuffer(
                    soft_body->ref->indices_for_rendering.size(), 
                    soft_body->ref->indices_for_rendering.data(),
                    soft_body->amt_vertices,
                    soft_body->deformed_vertices
                    );
            }

            void drawSceneToBackbuffer(scene::Scene *scene, cam::Camera *cam)
            {
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                math::m4 view = cam::genLookAt(cam);
                math::m4 proj = cam::genProjection(cam);                

                //render rigid bodies
                //TODO use instanced rendering per model
                {
                    Program prog = programs.rigid_body_shading;
                    glUseProgram(prog.id);

                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::VIEW], 1, GL_FALSE, glm::value_ptr(view));                
                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::PROJECTION], 1, GL_FALSE, glm::value_ptr(proj));      

                    glUniform3f(prog.unif_locations[UNIFORMS::POINT_LIGHT_1_POS], scene->point_light_1.x, scene->point_light_1.y, scene->point_light_1.z);

                    for(u32 i = 0; i < scene->rigid_bodies_to_render_ptr.size(); ++i)
                    {
                        sim::RigidBody *rigid_body = scene->rigid_bodies_to_render_ptr[i];
                        assets::triangle::TriangleMesh *current_mesh = rigid_body->ref;                        
                        u32 mesh_id = current_mesh->id;

                        math::m4 model_transform = sim::toM4(rigid_body);
                        glUniformMatrix4fv(prog.unif_locations[UNIFORMS::MODEL_TRANSFORM], 1, GL_FALSE, glm::value_ptr(model_transform));      
                        glUniform4f(
                            prog.unif_locations[UNIFORMS::COLOR], 
                            rigid_body->color.r, rigid_body->color.g, rigid_body->color.b, rigid_body->color.a
                        ); 

                        u32 vao = gpu_memory.triangle_mesh_ids_to_vao[mesh_id];
    
                        glBindVertexArray(vao);                        
                        glDrawElements(GL_TRIANGLES, current_mesh->amt_indices, GL_UNSIGNED_INT, 0);
                    }

                    glBindVertexArray(0);
                }                

                //render soft bodies
                //TODO see if we want to reuse the position buffers and measure if that makes
                //a difference for performance
                {
                    Program prog = programs.soft_body_shading;
                    glUseProgram(prog.id);

                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::VIEW], 1, GL_FALSE, glm::value_ptr(view));                
                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::PROJECTION], 1, GL_FALSE, glm::value_ptr(proj));         

                    for(u32 i = 0; i < scene->soft_bodies_to_render_ptr.size(); ++i)
                    {
                        sim::SoftBody *soft_body = scene->soft_bodies_to_render_ptr[i];
                        assets::teth::TethMesh *current_mesh = soft_body->ref;

                        const auto& iter = gpu_memory.teth_mesh_ids_to_index_buffer.find(current_mesh->id);
                        assert(iter !=  gpu_memory.teth_mesh_ids_to_index_buffer.end());

                        u32 ebo = iter->second;
                        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

                        u32 pos_buffer = pushVerticesOntoGPUReturnBuffer(soft_body->deformed_vertices, soft_body->amt_vertices);
                        u32 normal_buffer = calculateSoftBodyNormalVectorsPushOnGPUReturnBuffer(soft_body);

                        glUniform3f(prog.unif_locations[UNIFORMS::COLOR], soft_body->color.r, soft_body->color.g, soft_body->color.b);         

                        glDrawElements(GL_TRIANGLES, current_mesh->indices_for_rendering.size(), GL_UNSIGNED_INT, 0);

                        glDeleteBuffers(1, &normal_buffer);
                        glDeleteBuffers(1, &pos_buffer);
                    }
                }                
                
                //render rectangles
                {
                    Program prog = programs.draw_points;
                    glUseProgram(prog.id);

                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::VIEW], 1, GL_FALSE, glm::value_ptr(view));                
                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::PROJECTION], 1, GL_FALSE, glm::value_ptr(proj));     
                    
                    auto render_func = [](math::Rect rect, Program prog)
                    {
                        glUniform3f(prog.unif_locations[UNIFORMS::COLOR], rect.color.r, rect.color.g, rect.color.b);         

                        math::v3 points[] = 
                        {
                            rect.min,
                            {rect.min.x, rect.min.y, rect.max.z},

                            rect.min,
                            {rect.min.x, rect.max.y, rect.min.z},

                            rect.min,
                            {rect.max.x, rect.min.y, rect.min.z},

                            rect.max,
                            {rect.max.x, rect.max.y, rect.min.z},

                            rect.max,
                            {rect.max.x, rect.min.y, rect.max.z},

                            rect.max,
                            {rect.min.x, rect.max.y, rect.max.z}, 

                            {rect.min.x, rect.min.y, rect.max.z},
                            {rect.min.x, rect.max.y, rect.max.z},   

                            {rect.min.x, rect.min.y, rect.max.z},
                            {rect.max.x, rect.min.y, rect.max.z},

                            {rect.max.x, rect.min.y, rect.min.z},
                            {rect.max.x, rect.min.y, rect.max.z},

                            {rect.max.x, rect.min.y, rect.min.z},
                            {rect.max.x, rect.max.y, rect.min.z},

                            {rect.min.x, rect.max.y, rect.min.z},
                            {rect.max.x, rect.max.y, rect.min.z},

                            {rect.min.x, rect.max.y, rect.min.z},
                            {rect.min.x, rect.max.y, rect.max.z},   
                        };                           
                            
                        u32 pos_buffer = pushVerticesOntoGPUReturnBuffer(points, 12 * 2);
                        glDrawArrays(GL_LINES, 0, 12 * 2);
                        glDeleteBuffers(1, &pos_buffer);

                    };

                    for(u32 i = 0; i < scene->rects.size(); ++i)
                    {         
                        math::Rect rect = scene->rects[i];
                        render_func(rect, prog);
                    }
                    
                    for(u32 i = 0; i < scene->debug_rects.size(); ++i)
                    {         
                        math::Rect rect = scene->debug_rects[i];
                        render_func(rect, prog);
                    }
                }
                
                //TODO figure out how to render rays, maybe a strechy cylinder
                {
                    Program prog = programs.draw_points;
                    glUseProgram(prog.id);

                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::VIEW], 1, GL_FALSE, glm::value_ptr(view));                
                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::PROJECTION], 1, GL_FALSE, glm::value_ptr(proj));    

                    for(u32 i = 0; i < scene->debug_lines.size(); ++i)
                    {
                        glUniform3f(prog.unif_locations[UNIFORMS::COLOR], 1, 0, 0);         

                        scene::Line line = scene->debug_lines[i];

                        math::v3 points[] = {
                            line.p1,
                            line.p2               
                        };

                        u32 pos_buffer = pushVerticesOntoGPUReturnBuffer(points, 2);

                        glUniform3f(prog.unif_locations[UNIFORMS::COLOR], line.color.r, line.color.g, line.color.b);

                        glDrawArrays(GL_LINES, 0, 2);
                        glDeleteBuffers(1, &pos_buffer);
                    }
                }

                //render points
                { 
                    Program prog = programs.draw_points;
                    glUseProgram(prog.id);

                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::VIEW], 1, GL_FALSE, glm::value_ptr(view));                
                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::PROJECTION], 1, GL_FALSE, glm::value_ptr(proj));    

                    for(u32 i = 0; i < scene->debug_points.size(); ++i)
                    {
                        math::Point p = scene->debug_points[i];
                        u32 pos_buffer = pushVerticesOntoGPUReturnBuffer(&p.pos, 1);
                        glUniform3f(prog.unif_locations[UNIFORMS::COLOR], p.color.r, p.color.g, p.color.b);
                        
                        glDrawArrays(GL_POINTS, 0, 1);
                        glDeleteBuffers(1, &pos_buffer);
                    }
                }

                //render triangles
                { 
                    Program prog = programs.draw_points;
                    glUseProgram(prog.id);

                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::VIEW], 1, GL_FALSE, glm::value_ptr(view));                
                    glUniformMatrix4fv(prog.unif_locations[UNIFORMS::PROJECTION], 1, GL_FALSE, glm::value_ptr(proj));    

                    for(u32 i = 0; i < scene->debug_triangles.size(); ++i)
                    {
                        math::Triangle t = scene->debug_triangles[i];
                        u32 pos_buffer = pushVerticesOntoGPUReturnBuffer(&t.p1, 3);
                        glUniform3f(prog.unif_locations[UNIFORMS::COLOR], t.color.r, t.color.g, t.color.b);
                        
                        glDrawArrays(GL_TRIANGLES, 0, 3);

                        glDeleteBuffers(1, &pos_buffer);
                    }
                }
                

#if 0

                prog = programs.draw_points;
                glUseProgram(prog.id);

                glUniformMatrix4fv(prog.unif_locations[UNIFORMS::VIEW], 1, GL_FALSE, glm::value_ptr(view));                
                glUniformMatrix4fv(prog.unif_locations[UNIFORMS::PROJECTION], 1, GL_FALSE, glm::value_ptr(proj));     

                for(u32 i = 0; i < scene->rays.size(); ++i)
                {                    
                    math::v3 points[] = {
                        scene->rays[i].origin,
                        scene->rays[i].origin + scene->rays[i].dir,
                        scene->rays[i].origin + scene->rays[i].dir + math::v3{1},                        
                    };
                    
                    u32 pos_buffer = pushVerticesOntoGPUReturnBuffer(points, 3);
                    glDrawArrays(GL_TRIANGLES, 0, 3);
                    glDeleteBuffers(1, &pos_buffer);
                }
#endif 
            }
        }        
    }
}