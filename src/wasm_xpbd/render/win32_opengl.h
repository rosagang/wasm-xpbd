#pragma once
#include <assert.h>

#define GLEW_NO_GLU
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include <typedefs.h>

#include "./opengl.h"
#include "./scene.h"


namespace RED
{
    namespace render
    {
        SDL_Window* createWindowAndInitOpenGL(const char *title, f32 w, f32 h)
        {
            SDL_Init(SDL_INIT_EVERYTHING);

            SDL_Window *window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w, h, SDL_WINDOW_OPENGL);
            assert(window);
            SDL_GLContext ctx = SDL_GL_CreateContext(window);
            SDL_GL_MakeCurrent(window, ctx);

            glewInit();

            glEnable(GL_DEPTH_TEST);
            glViewport(0, 0, w, h);


            ogl3::setupAfterCtxCreation(w, h);

            return window;
        }

        void renderSceneAndSwapWindows(SDL_Window *window, scene::Scene *scene, cam::Camera *cam)
        {
            ogl3::drawSceneToBackbuffer(scene, cam);
            SDL_GL_SwapWindow(window);
        }

    }
}
