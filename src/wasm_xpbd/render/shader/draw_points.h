#pragma once

namespace RED
{
    namespace render
    {
        namespace draw_points
        {
            const char *VERT = 
R"glsl(
#version 100
precision mediump float;

attribute vec3 a_pos;

uniform mat4 projection;
uniform mat4 view;


void main()
{
    gl_Position = projection * view * vec4(a_pos, 1.); 
}

)glsl";

            const char *FRAG = 

R"glsl(
#version 100
precision mediump float;

uniform vec3 color;

void main()
{   
    gl_FragColor = vec4(color, 1.);
}
)glsl";

        }
    }
}