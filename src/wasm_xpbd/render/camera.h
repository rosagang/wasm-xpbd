#pragma once

#include "../maths/maths.h"

namespace RED
{
    namespace render
    {
        namespace cam 
        {
            const glm::vec3 WORLD_UP{0., 1., 0.};

            struct Camera
            {
                glm::vec3 pos;
                glm::vec3 view_dir;
                glm::vec3 right;
                glm::vec3 up;
                f32 w;
                f32 h;
                f32 fov;
            };

            void setupCamera(Camera *camera, glm::vec3 pos, glm::vec3 view_dir, f32 w, f32 h, f32 fov)
            {
                camera->pos = pos;
                camera->view_dir = glm::normalize(view_dir);
                camera->right = glm::normalize(glm::cross(view_dir, WORLD_UP));
                camera->up = glm::normalize(glm::cross(camera->right, camera->view_dir));
                camera->w = w;
                camera->h = h;
                camera->fov = fov;
            }
            math::Ray castRay(cam::Camera *cam, math::v2 xy_screen)
            {   
                math::Ray ray{};

                ray.origin = cam->pos;
                xy_screen.x /= cam->w;
                xy_screen.y /= cam->h;    

                xy_screen -= .5;
                xy_screen *= 2.;

                xy_screen.y *= -1;
                //xy is now from lower left to upper right: -1 to 1

                xy_screen.x *= (cam->w / cam->h);

                f32 dist_where_height_is_2 = 1. / tan(cam->fov / 2.); 

                math::v3 ray_dir = cam->view_dir * dist_where_height_is_2 + cam->up * xy_screen.y + cam->right * xy_screen.x;

                ray.dir = ray_dir;

                return ray;
            }

            glm::mat4 genLookAt(Camera *camera)
            {
                return glm::lookAt(camera->pos, camera->pos + camera->view_dir, camera->up);
            }

            math::m4 genProjection(Camera *camera)
            {
                return glm::perspective<f32>(camera->fov, camera->w / camera->h, .1f, 100.f);
            }

            void move(Camera *camera, glm::vec4 dirs, f32 dt)
            {
                if(glm::length(dirs) == 0) return;
                
                glm::vec4 dirsNorm = glm::normalize(dirs);

                f32 v = 10.;

                dirsNorm *= dt * v;

                camera->pos += camera->view_dir * dirsNorm[0];
                camera->pos -= camera->view_dir * dirsNorm[1];
                
                camera->pos -= camera->right * dirsNorm[2];
                camera->pos += camera->right * dirsNorm[3];
            }

            void rotate(Camera *camera, glm::vec2 xy, f32 dt)
            {
                if(glm::length(xy) == 0) return;

                glm::vec2 xyNorm = glm::normalize(xy);
                xyNorm *= dt;

                glm::mat4 rot = glm::rotate(glm::mat4{1.}, -xyNorm.x, camera->up);
                camera->view_dir = glm::normalize(glm::mat3(rot) * camera->view_dir);
                camera->right = glm::normalize(glm::cross(camera->view_dir, WORLD_UP));

                rot = glm::rotate(glm::mat4{1.}, -xyNorm.y, camera->right);
                camera->view_dir = glm::normalize(glm::mat3(rot) * camera->view_dir);
                camera->up = glm::normalize(glm::cross(camera->right, camera->view_dir));
            }
        }        
    }
}