#pragma once

#include <typedefs.h>

#include "../maths/maths.h"

namespace RED
{
    namespace sim
    {
        struct RayTriangleIntersectionData
        {
            b32 hit;
            f32 t;
            math::v3 bary_coords;
        };

        math::v3 baryCoordsToPos(math::v3 bary_coords, math::v3 a, math::v3 b, math::v3 c)
        {
            return  bary_coords.x * a +
                    bary_coords.y * b +
                    bary_coords.z * c; 
        }

        //https://gamedev.stackexchange.com/questions/23743/whats-the-most-efficient-way-to-find-barycentric-coordinates
        math::v3 findBaryCoords(math::v3 p, math::v3 a, math::v3 b, math::v3 c)
        {
            math::v3 v0 = b - a, v1 = c - a, v2 = p - a;
            float d00 = glm::dot(v0, v0);
            float d01 = glm::dot(v0, v1);
            float d11 = glm::dot(v1, v1);
            float d20 = glm::dot(v2, v0);
            float d21 = glm::dot(v2, v1);
            float denom = d00 * d11 - d01 * d01;
            f32 v = (d11 * d20 - d01 * d21) / denom;
            f32 w = (d00 * d21 - d01 * d20) / denom;
            f32 u = 1.0f - v - w;

            return {u, v, w};
        }

        RayTriangleIntersectionData calcRayTriangleIntersectionData(math::Ray ray, math::v3 p1, math::v3 p2, math::v3 p3, f32 max_dist)
        {
            RayTriangleIntersectionData res{};            

            math::v3 p1p2 = p2 - p1;
            math::v3 p1p3 = p3 - p1;

            math::v3 normal = glm::cross(p1p2, p1p3);

            //calc line-plane intersection

            f32 v = dot(ray.dir, normal);

            if(v == 0.)
            {
                return res;
            }

            f32 t = glm::dot((p1 - ray.origin), normal) / v;

            if(t >= max_dist)
            {
                //we triangle further away then max_dist
                return res;
            }

            if(t < 0.)
            {
                //triangle is behind the ray
                return res;
            }

            if(t == 0.)
            {
                //triangle and ray are parallel. They might still hit,
                //but for now we ignore this case
                return res;
            }

            math::v3 ray_plane_intersection_point = ray.origin + t * ray.dir;

            math::v3 bary_coords = findBaryCoords(ray_plane_intersection_point, p1, p2, p3);

            if(
                abs(bary_coords.x + bary_coords.y + bary_coords.z - 1.) > 1e-5  || 
                bary_coords.x < 0. || bary_coords.y < 0. || bary_coords.z < 0.)
            {
                return res;
            }

            res.hit = true;
            res.bary_coords = bary_coords;
            res.t = t;

            return res;
        }
    }
}