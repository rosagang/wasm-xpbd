#pragma once

#include <unordered_map>
#include <vector>

#include <typedefs.h>

#include "./phyiscs_bodies/soft_body.h"
#include "./phyiscs_bodies/rigid_body.h"

#include "./intersection/aabb.h"
#include "./intersection/aabb_spatial_hash_bucket_grid.h"
#include "./intersection/aabb_spatial_hash_grid.h"
#include "./intersection/gjk.h"

#include "./ray_triangle.h"
#include "./grab.h"
//#include "./sim_debug.h"
#include "./spatial_hash_grid.h"

#include "../assets/assets.h"
#include "../assets/model_3d/teth_mesh.h"
#include "../assets/model_3d/triangle_mesh.h"

#include "../render/scene.h"


namespace RED
{
    namespace sim
    {
        const math::v3 G{0., -9.8, 0.};


        struct ParticleRef
        {
            math::v3 *pos;
            f32 w;
        };

        struct Sim
        {
            const u32 MAX_AMT_RIGID_BODIES = 1024;
            RigidBody *rigid_bodies;
            u32 amt_rigid_bodies = 0;
            std::unordered_map<id::PhysBodyId, u64> ids_to_rigid_body_indices;

            //aabb::bucket_hash_grid::AABBSpatialGridBuckets aabb_bucket_hash_grid;
            aabb::AABBHashGrid aabb_hash_grid;

            grab::Grab *grab_ptr;  
            
            std::vector<coll::WALL_Collider> wall_colliders;
            std::vector<coll::CUBE_Collider> cube_colliders;

            b32 grav = true;

            void push_back_rigid_body(RigidBody rb, u32 id, assets::TRI_MESH_IDS mesh_id)
            {
                ids_to_rigid_body_indices.insert({id, amt_rigid_bodies});
                switch(mesh_id)
                {
                    case assets::TRI_MESH_IDS::PLANE:
                    {
                        wall_colliders.push_back({rigid_bodies + amt_rigid_bodies});
                        break;
                    }
                    case assets::TRI_MESH_IDS::CUBE:
                    {
                        cube_colliders.push_back({rigid_bodies + amt_rigid_bodies});
                        break;
                    }
                }
                assert(amt_rigid_bodies < MAX_AMT_RIGID_BODIES);
                rigid_bodies[amt_rigid_bodies++] = rb;                
            }
        };

        void init(Sim *sim, grab::Grab *grab_ptr)
        {
            sim->rigid_bodies = (RigidBody *) malloc(sim->MAX_AMT_RIGID_BODIES * sizeof(RigidBody));
            sim->grab_ptr = grab_ptr;
            id::init();
            aabb::init(&sim->aabb_hash_grid);
        }          

        RigidBody *getRigidBodyFromId(Sim *sim, u32 id)
        {
            u32 index = sim->ids_to_rigid_body_indices[id];
            return sim->rigid_bodies + index;
        }

        RigidBody *addRigidBodyFromTriangleMesh(
            Sim *sim, 
            assets::triangle::TriangleMesh *tri_mesh, 
            math::v3 pos, 
            math::quat quat, 
            f32 inv_mass,
            math::v3 scaling,
            math::v3 color)
        {
            u32 id = id::genRigidBodyId(); 
            RigidBody rb = generateFromTriangleMesh(tri_mesh, pos, quat, inv_mass, scaling, color, id);            
            sim->push_back_rigid_body(rb, id, (RED::assets::TRI_MESH_IDS) tri_mesh->id);             
            return sim->rigid_bodies + sim->amt_rigid_bodies - 1;
        }              
       
        //TODO use spatial hashing or a quadtree or whatever
        u32 getFirstTriangleUnderRayReturnPhysicalBodyId(
            Sim *sim,
            math::Ray ray, 
            math::uv3 *out_indices, 
            math::v3 *out_bary_coords, 
            f32 *dist_along_ray)
        {
            math::uv3 min_dist_triangle{};
            f32 min_dist = 10000.f;

            u32 hit_body_id = 0;

            for(u32 i = 0; i < sim->amt_rigid_bodies; ++i)
            {
                RigidBody& rigid_body = sim->rigid_bodies[i];
                
                if(rigid_body.inv_mass == 0.f) continue;

                math::m4 transform = toM4(&rigid_body);

                for(u32 triangleIdx = 0; triangleIdx < rigid_body.ref->amt_indices; triangleIdx += 3)
                {
                    u32 i1 = rigid_body.ref->indices[triangleIdx];
                    u32 i2 = rigid_body.ref->indices[triangleIdx + 1];
                    u32 i3 = rigid_body.ref->indices[triangleIdx + 2];

                    math::v3 p1 = rigid_body.ref->vert_positions[i1];
                    math::v3 p2 = rigid_body.ref->vert_positions[i2];
                    math::v3 p3 = rigid_body.ref->vert_positions[i3];

                    p1 = math::applyTransform(transform, p1);
                    p2 = math::applyTransform(transform, p2);
                    p3 = math::applyTransform(transform, p3);

#if 0
                    debug::debugPushPoint(sim->debug, math::v3(1.), p1);
                    debug::debugPushPoint(sim->debug, math::v3(1.), p2);
                    debug::debugPushPoint(sim->debug, math::v3(1.), p3);
                    debug::inputAndRender(sim->debug);                    
#endif

                    RayTriangleIntersectionData intersectionData = calcRayTriangleIntersectionData(ray, p1, p2, p3, min_dist);
                    if(intersectionData.hit)
                    {
                        min_dist_triangle = {intersectionData.bary_coords.x, intersectionData.bary_coords.y, intersectionData.bary_coords.z};
                        min_dist = intersectionData.t;
                        hit_body_id = rigid_body.id;
                        *out_indices = {i1, i2, i3};
                        *out_bary_coords = intersectionData.bary_coords;
                        *dist_along_ray = intersectionData.t;
                    }                    
                }
            }
            return hit_body_id;
        }
                      
    }
}
