#pragma once

#include <typedefs.h>

#include "./xpbd.h"
#include "./grab.h"

#include "../maths/maths.h"

#include "../input/input.h"

#include "../render/camera.h"
#include "../render/scene.h"


namespace RED
{
    namespace sim
    {
        namespace grab
        {

            void handleGrabCommands(
                input::Input input, grab::Grab *grab, 
                Sim *sim, render::cam::Camera *cam, 
                render::scene::Scene *scene)
            {
                switch(input.grab_command.action)
                {
                    case input::GRAB_ACTION::NOTHING:
                    {  
                        break;
                    }

                    case input::GRAB_ACTION::START:
                    {
                        RED::math::Ray ray = RED::render::cam::castRay(cam, input.grab_command.xy_screen);

                        RED::math::uv3 indices;
                        RED::math::v3 bary_coords;
                        f32 dist_along_ray;
                        math::v3 *hit_body_verts;

                        u32 hit_body_id = RED::sim::getFirstTriangleUnderRayReturnPhysicalBodyId(
                            sim, ray, &indices, &bary_coords, &dist_along_ray);


                        if(id::validId(hit_body_id))
                        {
                            RigidBody *rb = getRigidBodyFromId(sim, hit_body_id);
                            *grab = {
                                true,

                                indices,
                                bary_coords,

                                hit_body_id,
                                dist_along_ray,
                                ray.origin + dist_along_ray * ray.dir,
                                0.f
                            };
                        }

                        break;
                    }

                    case input::GRAB_ACTION::DRAG:
                    {
                        RED::math::Ray ray = RED::render::cam::castRay(cam, input.grab_command.xy_screen);
                        grab->current_hand_pos = ray.origin + grab->dist_along_ray * ray.dir;     
                        break;
                    }

                    case input::GRAB_ACTION::END:
                    {
                        grab->is_active = false;
                        break;
                    }
                }
            }
        }
    }
}