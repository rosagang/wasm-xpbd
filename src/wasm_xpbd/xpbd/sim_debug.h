#pragma once

#include <typedefs.h>

#include "./intersection/aabb.h"

#include "../maths/maths.h"

#include "../render/scene.h"
#include "../render/opengl.h"
#include "../render/win32_opengl.h"

#include "../render/camera.h"
#include "../input/sdl_input.h"


namespace RED
{
    namespace sim
    {
        namespace debug
        {
            struct SimDebug
            {
                SDL_Window *window;
                render::scene::Scene *scene;
                render::cam::Camera *cam;

                input::SDL_Input sdl_input;
            };            

            SimDebug debug;
            
            SimDebug init(
                SDL_Window *window_ptr, 
                render::scene::Scene *scene_ptr, 
                render::cam::Camera *cam_ptr)
            {

                SimDebug sim_debug{};

                sim_debug.cam = cam_ptr;
                sim_debug.window = window_ptr;
                sim_debug.scene = scene_ptr;
                sim_debug.sdl_input = input::setup();

                debug = sim_debug;

                return sim_debug;
            };
            
            void inputAndRender(b32 clear_on_continue = true)
            {
                for(;;)
                {
                    render::renderSceneAndSwapWindows(debug.window, debug.scene, debug.cam);

                    input::Input input = input::generateFromSDL(&debug.sdl_input);
                    RED::render::cam::move(debug.cam, RED::input::getMoveVec(input), 1. / 60.);
                    RED::render::cam::rotate(debug.cam, input.rotCameraCmds.xy, 1. / 60.);

                    if(input.step_once || input.step)
                    {         
                        if(clear_on_continue)
                        {
                            debug.scene->debug_lines.clear();
                            debug.scene->debug_points.clear();
                            debug.scene->debug_rects.clear();
                            debug.scene->debug_triangles.clear();
                        }               
                        break;
                    }
                }
            }

            void debugPushRectAroundPoint(math::v3 c, math::v3 p)
            {
                debug.scene->debug_rects.push_back({
                    c,
                    p + math::v3(.1),
                    p - math::v3(.1)
                });
            }
        
            void debugPushPoint(math::v3 c, math::v3 p)
            {
                debug.scene->debug_points.push_back({c, p});
            }

            void debugPushLineInDir(math::v3 c, math::v3 p, math::v3 dir)
            {
                debug.scene->debug_lines.push_back({c, p, p + dir});
            }

            void pushAABB(math::v3 c, math::v3 *pos, u32 amt_pos)
            {
                aabb::AABB aabb = aabb::calcAABB(pos, amt_pos);   
                debug.scene->debug_rects.push_back({c, aabb.min, aabb.max});
            }

            void pushRigidBodyAABB(math::v3 c, RigidBody *rb)
            {
                aabb::AABB aabb = aabb::calcAABB(toM4(rb), rb->ref->vert_positions, rb->ref->amt_verts);   
                debug.scene->debug_rects.push_back({c, aabb.min, aabb.max});
            }

            void pushTriangle(math::v3 c, math::v3 p1, math::v3 p2, math::v3 p3, b32 draw_normal = true)
            {
                debug.scene->debug_triangles.push_back({math::v3(0, 1, 0), p1, p2, p3});

                debug.scene->debug_lines.push_back({math::v3(0), p1, p2});
                debug.scene->debug_lines.push_back({math::v3(0), p2, p3});
                debug.scene->debug_lines.push_back({math::v3(0), p3, p1});

                math::v3 n = glm::cross(p2 - p1, p3 - p1);
                math::v3 mid = p1 + .3f * (p2 - p1) + .3f * (p3 - p1);
                if(draw_normal)
                    debugPushLineInDir({.3, 0., 0.}, mid, n);                
            }

            
        }
    }
}