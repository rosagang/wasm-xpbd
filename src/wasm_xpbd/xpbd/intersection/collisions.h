#pragma once

#include <typedefs.h>

#include "../sim.h"

#include "../../maths/maths.h"



namespace RED
{
    namespace sim
    {
        namespace coll
        {
            Sim *sim_ptr;

            struct Collision
            {
                gjk::EPARes epa_res;
                RigidBody* lhs;
                RigidBody* rhs;
                f32 vn_lin_prev;
                f32 vn_rot_prev;
                math::v3 lhs_gained_vel;
                math::v3 rhs_gained_vel;
            };

            struct CollisionArr
            {
                std::vector<Collision> colls;
            
                void push_back(Collision c)
                {
                    colls.push_back(c);
                }

                void remove_at(u32 idx)
                {
                    colls[idx] = colls[colls.size() - 1];
                    colls.pop_back();
                }
            };

            b32 rigidBodyWallCollision(RigidBody *rb, RigidBody *wall, gjk::EPARes *epa_res)
            {
                math::m4 wall_transform = toM4(wall);

                math::v3 p1 = math::applyTransform(wall_transform, wall->ref->vert_positions[wall->ref->indices[0]]);
                math::v3 p2 = math::applyTransform(wall_transform, wall->ref->vert_positions[wall->ref->indices[1]]);
                math::v3 p3 = math::applyTransform(wall_transform, wall->ref->vert_positions[wall->ref->indices[2]]);
                math::v3 p = .33f * p3 + .33f * p2 + .33f * p1;
                math::v3 n = -glm::normalize(glm::cross(p2 - p1, p3 - p1));

                if(abs(n.x) > .2)
                {
                    int j = 0;
                }


                math::m4 rb_transform = toM4(rb);
                

                f32 min = std::numeric_limits<f32>::max();
                math::v3 min_point{};
                for(u32 i = 0; i < rb->ref->amt_verts; ++i)
                {
                    math::v3 pos = math::applyTransform(rb_transform, rb->ref->vert_positions[i]);
                    f32 d = glm::dot(pos - p, n);
                    if(d < min)
                    {
                        min = d;
                        min_point = pos;
                    }
                }

                if(min < 0.)
                {
                    epa_res->n = -n;
                    epa_res->c = -min;
                    math::v3 r1 = glm::transpose(rb->current_rot_data.current_rot_mat) * (min_point - rb->pos);
                    math::v3 r2 = min_point - min * n;
                    r2 = glm::transpose(wall->current_rot_data.current_rot_mat) * (r2 - wall->pos);
                    epa_res->r1 = r1;
                    epa_res->r2 = r2;
                    return true;
                }

                return false;
            }

            void findAllCurrentCollisions(Sim *sim, CollisionArr *out_colls)
            {
                for(u32 i = 0; i < sim->cube_colliders.size(); ++i)
                {
                    RigidBody *lhs = sim->cube_colliders[i].rb;
                    for(u32 j = 0; j < sim->wall_colliders.size(); ++j)
                    {
                        RigidBody *wall = sim->wall_colliders[j].rb;
                        gjk::EPARes res;
                        if(rigidBodyWallCollision(lhs, wall, &res))
                        {
                            out_colls->push_back({res, lhs, wall});
                        }
                    }      
                    for(u32 j = i + 1; j < sim->cube_colliders.size(); ++j)
                    {
                        RigidBody *rhs = sim->cube_colliders[j].rb;
                        gjk::EPARes res;
                        if(gjk::performRigidBodyGJK(lhs, rhs, &res))
                        {
                            out_colls->push_back({res, lhs, rhs});
                        }
                    }
                }
            }

            u32 findPossibleCollsWithWalls(RigidBody *rb, Sim *sim, Collision *out_coll)
            {
                if(rb->inv_mass == 0.) return 0;
                u32 found = 0;
                for(u32 j = 0; j < sim->wall_colliders.size(); ++j)
                {
                    RigidBody *wall = sim->wall_colliders[j].rb;
                    gjk::EPARes res;
                    if(rigidBodyWallCollision(rb, wall, &res))
                    {
                        out_coll[found++] = {res, rb, wall};
                    }
                }          
                return found;
            }
            
            u32 findFirstCollisionForRB(RigidBody *rb, Sim *sim, Collision *out_coll, u32 amt)
            {      
                if(rb->inv_mass == 0.) return 0; 

                aabb::AABB rb_aabb = aabb::calcAABB(toM4(rb), rb->ref->vert_positions, rb->ref->amt_verts);

                u32 found = 0;

                for(u32 j = 0; j < sim->wall_colliders.size() && found < amt; ++j)
                {
                    RigidBody *wall = sim->wall_colliders[j].rb;
                    gjk::EPARes res;
                    if(rigidBodyWallCollision(rb, wall, &res))
                    {
                        out_coll[found++] = {res, rb, wall};
                    }
                }          

                std::unordered_set<RigidBody *> poss_colls{};// = sim->aabb_hash_grid.query(rb_aabb);

                for(RigidBody *rhs : poss_colls)
                {
                    if(rhs == rb) continue;
                    aabb::AABB rhs_aabb = aabb::calcAABB(toM4(rhs), rhs->ref->vert_positions, rhs->ref->amt_verts);
                    if(!aabb::overlapp(rb_aabb, rhs_aabb)) continue;
                    gjk::EPARes res;
                    if(gjk::performRigidBodyGJK(rb, rhs, &res))
                    {
                        out_coll[found++] = {res, rb, rhs};
                    }
                    if(found == amt) break;
                }
#if 0
                for(u32 i = 0; i < sim->cube_colliders.size() && found < amt; ++i)
                {
                    RigidBody *rhs_cube = sim->cube_colliders[i].rb;

                    if(rb == rhs_cube) continue;
                    
                    aabb::AABB rhs_aabb = aabb::calcAABB(toM4(rhs_cube), rhs_cube->ref->vert_positions, rhs_cube->ref->amt_verts);

                    if(!aabb::overlapp(rb_aabb, rhs_aabb)) continue;

                    gjk::EPARes res;
                    if(gjk::performRigidBodyGJK(rb, rhs_cube, &res))
                    {
                        out_coll[found++] = {res, rb, rhs_cube};
                    }
                }   
#endif

                return found;
            }
        }
    }
}