#pragma once

#include <typedefs.h>

#include "../phyiscs_bodies/rigid_body.h"

#include "../../maths/maths.h"

namespace RED
{
    namespace sim
    {
        namespace coll
        {

            struct CUBE_Collider
            {
                RigidBody *rb;
            };

            struct WALL_Collider
            {
                RigidBody *rb;
            };
        }
    }
}