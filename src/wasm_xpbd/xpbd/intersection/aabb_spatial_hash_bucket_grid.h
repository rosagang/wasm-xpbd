#pragma once

#include <unordered_set>

#include <typedefs.h>

#include "./aabb.h"

//#include "../sim_debug.h"

#include "../../maths/maths.h"

#include "../../memory/memory.h"

namespace RED
{
   
    namespace sim
    {
        namespace aabb
        {
            namespace bucket_hash_grid
            {
                const u32 MAX_ENTRIES_IN_CELL = 512;
                const u32 MAX_CELLS_IN_BUCKET = 32;
                const u32 AMT_BUCKETS = 1024;

                const u32 MAX_CELLS_TOTAL = MAX_CELLS_IN_BUCKET * AMT_BUCKETS;
                const u32 MAX_ENTRIES_TOTAL = MAX_CELLS_TOTAL * MAX_ENTRIES_IN_CELL;

                const f32 SPACING = .5;

                struct HashGridCell
                {
                    u32 size;
                    u32 ids[MAX_ENTRIES_IN_CELL];

                    void append(u32 id)
                    {
                        assert(size < MAX_ENTRIES_IN_CELL);
                        ids[size] = id;
                        size++;
                    }

                    void remove(u32 id)
                    {
                        for(u32 i = 0; i < size; ++i)
                        {
                            if(ids[i] == id)
                            {
                                ids[i] = ids[size - 1];
                                size--;
                                return;
                            }
                        }
                    }

                    void print()
                    {
                        for(u32 i = 0; i < size; ++i)
                        {
                            printf(", %u", ids[i]);
                        }
                        printf("\n");
                    }
                };

                struct CellArenaStorage
                {
                    HashGridCell *cells;
                    HashGridCell *last_filled_cell;
                    u32 amt_empty_cells;
                    u32 *empty_cells;

                    void init()
                    {
                        cells = mem::MEM_Z<HashGridCell>(MAX_CELLS_TOTAL);
                        last_filled_cell = cells;
                        empty_cells = mem::MEM<u32>(MAX_CELLS_TOTAL);
                        amt_empty_cells = 0;
                    }

                    void clear()
                    {
                        amt_empty_cells = 0;
                        last_filled_cell = cells;
                        memset(cells, 0, MAX_CELLS_TOTAL * sizeof(HashGridCell));
                    }

                    HashGridCell *alloc()
                    {
                        if(amt_empty_cells != 0)
                        {
                            amt_empty_cells--;
                            u32 empty_idx = empty_cells[amt_empty_cells];
                            return cells + empty_idx;
                        }
                        else
                        {
                            assert(last_filled_cell < cells + MAX_CELLS_TOTAL);
                            last_filled_cell++;
                            return last_filled_cell - 1;
                        }
                    }

                    void free(HashGridCell *c)
                    {
                        if (c == last_filled_cell - 1)
                        {
                            last_filled_cell -= 1;
                        }
                        else
                        {
                            u32 idx = c - cells;
                            empty_cells[amt_empty_cells] = idx;
                            amt_empty_cells++;
                        }
                    }
                };

                struct HashGridBucket
                {
                    u32 amt_cells;
                    math::iv3 cell_coordinates[MAX_CELLS_IN_BUCKET];
                    HashGridCell *cell_ptrs[MAX_CELLS_IN_BUCKET];

                    HashGridCell * getCellPtr(math::iv3 c_coords)
                    {
                        for(u32 i = 0; i < amt_cells; ++i)
                        {
                            if(cell_coordinates[i] == c_coords)
                            {
                                return cell_ptrs[i];
                            }
                        }
                        return 0;
                    }

                    void append(HashGridCell * c, math::iv3 c_coords)
                    {
                        assert(amt_cells < MAX_CELLS_IN_BUCKET);
                        cell_ptrs[amt_cells] = c;
                        cell_coordinates[amt_cells] = c_coords;
                        amt_cells++;
                    }        

                    void remove(HashGridCell * c)
                    {
                        for(u32 i = 0; i < amt_cells; ++i)
                        {
                            if(cell_ptrs[i] == c)
                            {
                                amt_cells--;
                                cell_ptrs[i] = cell_ptrs[amt_cells];
                                cell_coordinates[i] = cell_coordinates[amt_cells];
                                return; 
                            }
                        }
                    }

                };                

                struct Bucket_Cell
                {
                    HashGridBucket *b;
                    HashGridCell *c;
                    math::iv3 c_coords;

                    b32 operator==(const Bucket_Cell& other)
                    {
                        return b == other.b && c == other.c;
                    }
                };

                struct AABBSpatialGridBuckets
                { 
                    HashGridBucket *buckets;       
                    CellArenaStorage cell_storage; 

                    math::iv3 cell(math::v3 p)
                    {
                        return glm::floor(p / SPACING);
                    }   

                    u32 hash(math::iv3 c)
                    {
                        return ((c.x * 92837111) ^ (c.y * 689287499) ^ (c.z * 283923481)) % AMT_BUCKETS;	
                    }

                    std::vector<Bucket_Cell> enumerateCells(aabb::AABB aabb)
                    {
                        math::iv3 min_cell = cell(aabb.min);
                        math::uv3 amt_steps = cell(aabb.max) + 1 - min_cell;

                        u32 amt_cells = amt_steps.x * amt_steps.y * amt_steps.z;
                        
                        if(glm::length(math::v3(amt_steps)) > 10) return {};

                        std::vector<Bucket_Cell> res{};
                        res.reserve(124);

                        for(u32 z = 0; z < amt_steps.z; ++z)
                        {
                            for(u32 y = 0; y < amt_steps.y; ++y)
                            {
                                for(u32 x = 0; x < amt_steps.x; ++x)
                                {
                                    math::iv3 c_coords = min_cell + math::iv3(x, y, z);
                                    HashGridBucket *b = buckets + hash(c_coords);
                                    HashGridCell *c = b->getCellPtr(c_coords);
                                    if(!c)
                                    {
                                        c = cell_storage.alloc();
                                        b->append(c, c_coords);
                                    }
                                    res.push_back({b, c, c_coords});
                                }   
                            }    
                        }

                        return res;
                    }

                    void insert(u32 id, aabb::AABB aabb)
                    {
                        //debug::debug.scene->debug_rects.push_back({{0, 0, 1}, aabb.min, aabb.max});

                        std::vector<Bucket_Cell> bcs = enumerateCells(aabb);
                        for(const auto &bc :bcs)
                        {
                            bc.c->append(id);
                        }
                    }

                    void clear()
                    {
                        memset(buckets, 0, AMT_BUCKETS * sizeof(HashGridBucket));
                        cell_storage.clear();
                    }

                    void update(u32 id, aabb::AABB prev_aabb, aabb::AABB new_aabb)
                    {                     
                        std::vector<Bucket_Cell> prev_bcs = enumerateCells(prev_aabb);        
                        std::vector<Bucket_Cell> new_bcs = enumerateCells(new_aabb);           

                        for(const auto &bc : new_bcs)
                        {
                            bc.c->append(id);                           
                        }
                        
                        for(const auto &bc : prev_bcs)
                        {               
                            bc.c->remove(id);
                            if(bc.c->size == 0)
                            {                               
                                bc.b->remove(bc.c);
                                cell_storage.free(bc.c);
                            }                        
                        }                       
                    }

                    std::unordered_set<u32> query(u32 id, aabb::AABB aabb)
                    {
                        std::unordered_set<u32> res;
                        res.reserve(16);
                        std::vector<Bucket_Cell> bcs = enumerateCells(aabb);
                        for(const auto &bc :bcs)
                        {
                            for(u32 i = 0; i < bc.c->size; ++i)
                            {
                                if(bc.c->ids[i] != id)
                                {
                                    res.insert(bc.c->ids[i]);
                                } 
                            }
                        }
                        return res;
                    }

                    u32 queryFirst(u32 id, aabb::AABB aabb)
                    {
                        std::vector<Bucket_Cell> bcs = enumerateCells(aabb);
                        for(const auto &bc :bcs)
                        {
                            for(u32 i = 0; i < bc.c->size; ++i)
                            {
                                if(bc.c->ids[i] != id)
                                {
                                    return bc.c->ids[i];    
                                } 
                            }
                        }
                        return 0;
                    }

                    void remove(u32 id, aabb::AABB aabb)
                    {
                        std::vector<Bucket_Cell> bcs = enumerateCells(aabb);
                        for(const auto &bc : bcs)
                        {               
                            bc.c->remove(id);
                            if(bc.c->size == 0)
                            {                               
                                bc.b->remove(bc.c);
                                cell_storage.free(bc.c);
                            }                        
                        }        
                    }

                    void print()
                    {
                        for(HashGridBucket *b = buckets; b != buckets + AMT_BUCKETS; ++b)
                        {
                            u32 amt_cells = b->amt_cells;
                            if(amt_cells == 0) continue;
                            printf("amt cells %u\n", amt_cells);
                            for(u32 i = 0; i < amt_cells; ++i)
                            {
                                HashGridCell *c = b->cell_ptrs[i];
                                math::iv3 c_coords = b->cell_coordinates[i];
                                u32 amt_ids = c->size;
                                
                                printf("coords (%i,%i,%i) amt ids: %u :: ", c_coords.x, c_coords.y, c_coords.z, amt_ids);
                                for(u32 j = 0; j < amt_ids; ++j)
                                {
                                    u32 id = c->ids[j];
                                    printf(" %u ", id);
                                }
                                printf("\n");
                            }
                        }
                    }

                    void render()
                    {
                        for(HashGridBucket *b = buckets; b != buckets + AMT_BUCKETS; ++b)
                        {
                            u32 amt_cells = b->amt_cells;
                            if(amt_cells == 0) continue;
                            for(u32 i = 0; i < amt_cells; ++i)
                            {
                                HashGridCell *c = b->cell_ptrs[i];
                                math::iv3 c_coords = b->cell_coordinates[i];
                                u32 amt_ids = c->size;
                                if(amt_ids > 0)
                                {
                                    /*
                                    debug::debug.scene->debug_rects.push_back(
                                        {{0, 1, 0}, 
                                        math::v3(c_coords) * SPACING, 
                                        math::v3(c_coords) * SPACING + math::v3(SPACING)});
                                    */
                                }
                                for(u32 j = 0; j < amt_ids; ++j)
                                {
                                    u32 id = c->ids[j];
                                }
                            }
                        }
                    }

                };

            
                
                void init(AABBSpatialGridBuckets *grid)
                { 
                    HashGridBucket *bucket_bytes = mem::MEM_Z<HashGridBucket>(AMT_BUCKETS);
                    grid->buckets = bucket_bytes;
                    grid->cell_storage.init();                                                                          
                }
            }            
        }        
    }
}
  
