#pragma once

#include "../../maths/maths.h"

#include "../../assets/model_3d/triangle_mesh.h"

#include "../intersection/aabb.h"

#include "./body_id.h"

namespace RED
{
    namespace sim
    {        
        struct CurrentRotData
        {
            math::m3 current_I_inv;
            math::m3 current_rot_mat;
        };

        struct RigidBody
        {
            id::PhysBodyId id;

            //TODO move this somewhere else
            math::v4 color;

            assets::triangle::TriangleMesh *ref;

            //The pos param of a rigid body refers to where the com is located
            //This might not be 0 w.r.t the vertex positions of the body,
            //so for correct phyiscs (such as collision detection) and rendering,
            //it needs to be subtracted from the vertice's positions on the gpu
            math::v3 zero_pos_to_com_offset;
            
            math::v3 scaling;

            math::v3 pos;            
            math::v3 prev_pos;            
            math::v3 velocity; 

            math::quat quat;
            math::quat prev_quat;
            math::v3 angular_vel;

            math::m3 I;
            math::m3 inv_I;
            f32 inv_mass;

            CurrentRotData current_rot_data;
        };  

        math::quat flipQuatToAvoidJumps(math::quat old_q, math::quat new_q)
        {
            if(glm::length(old_q - new_q) < glm::length(old_q + new_q))
            {
                return -new_q;
            }
            else
            {
                return new_q;
            }
        }

        f32 calcRotationalMassInDir(RigidBody *rb, math::v3 r_rotated, math::v3 n_world_frame)
        {
            math::v3 rxn = glm::cross(r_rotated, n_world_frame);
            math::v3 inner_1 = rb->current_rot_data.current_I_inv * rxn;
            f32 inner = glm::dot(rxn, inner_1);
            return inner;
        }

        f32 calcGenInverseMassInDir(RigidBody *rb, math::v3 r_rotated, math::v3 n_world_frame)
        {
            f32 rot_m = calcRotationalMassInDir(rb, r_rotated, n_world_frame);
            f32 w1 = rb->inv_mass + rot_m;
            return w1;
        }

        void updateQuat(RigidBody *rb, math::quat q)
        {
            q = glm::normalize(q);

            q = flipQuatToAvoidJumps(rb->quat, q);

            rb->quat = q;
            math::m3 rot_mat = math::m3(glm::toMat4(q));
            math::m3 current_I_inv = rot_mat * rb->inv_I * glm::transpose(rot_mat);
            rb->current_rot_data.current_I_inv = current_I_inv;
            rb->current_rot_data.current_rot_mat = rot_mat;
        }

        math::v3 pointToBodyFrame(RigidBody *rb, math::v3 r)
        {
            return glm::transpose(rb->current_rot_data.current_rot_mat) * (r - rb->pos); 
        }

        math::m4 toM4(RigidBody *rigid_body)
        {
            math::m4 rot = glm::toMat4(rigid_body->quat);
            math::m4 translate = glm::translate(rigid_body->pos - rigid_body->zero_pos_to_com_offset);
            math::m4 scale = glm::scale(math::m4{1.}, rigid_body->scaling);

            math::m4 transform = translate * rot * scale;

            return transform;
        }

        math::m4 toM4NoScaling(RigidBody *rigid_body)
        {
            math::m4 rot = glm::toMat4(rigid_body->quat);
            math::m4 translate = glm::translate(rigid_body->pos - rigid_body->zero_pos_to_com_offset);

            math::m4 transform = translate * rot;

            return transform;
        }

        math::v3 posToWorld(RigidBody *rb, math::v3 pos)
        {
            math::m4 m4 = toM4(rb);
            math::v3 res = math::applyTransform(m4, pos);
            return res;
        }

        math::m3 calculateMeshInertiaMatrix(assets::triangle::TriangleMesh *mesh, math::v3 com_offset, f32 mass, math::v3 scaling)
		{
			f32 mass_per_point = mass / mesh->amt_verts;

			glm::mat3 inertia(0.);

			f32 inertia_x = 0;
			f32 inertia_xy = 0;
			f32 inertia_xz = 0;
			f32 inertia_y = 0;
			f32 inertia_yz = 0;
			f32 inertia_z = 0;

			for (u32 i = 0; i < mesh->amt_verts; ++i)
			{
				glm::vec3 mesh_vertex = mesh->vert_positions[i];
				mesh_vertex *= scaling;

				glm::vec3 vertex_from_com = mesh_vertex - com_offset;

				inertia_x += vertex_from_com.y * vertex_from_com.y + vertex_from_com.z * vertex_from_com.z;
				inertia_xy -= vertex_from_com.x * vertex_from_com.y;
				inertia_xz -= vertex_from_com.x * vertex_from_com.z;

				inertia_y += vertex_from_com.x * vertex_from_com.x + vertex_from_com.z * vertex_from_com.z;
				inertia_yz -= vertex_from_com.y * vertex_from_com.z;

				inertia_z += vertex_from_com.x * vertex_from_com.x + vertex_from_com.y * vertex_from_com.y;
			}

			inertia[0][0] = inertia_x;
			
			inertia[0][1] = inertia_xy;
			inertia[1][0] = inertia_xy;

			inertia[0][2] = inertia_xz;
			inertia[2][0] = inertia_xz;

			inertia[1][1] = inertia_y;

			inertia[1][2] = inertia_yz;
			inertia[2][1] = inertia_yz;

			inertia[2][2] = inertia_z;

			return mass_per_point * inertia;
		}

        math::v3 calculateMeshCOM(assets::triangle::TriangleMesh *mesh)
		{
			math::v3 com(0.);
			for (u32 i = 0; i < mesh->amt_verts; ++i)
			{
				com += mesh->vert_positions[i];
			}
			return com;
		}

        aabb::AABB calculateAABB(assets::triangle::TriangleMesh *mesh, math::v3 translation, math::quat quat, math::v3 scaling)
        {
            aabb::AABB aabb{};
            aabb.min = {1000000., 1000000., 1000000.};
            aabb.max = -aabb.min;

            math::m4 model_transform = glm::toMat4(glm::quat{quat});
            model_transform = glm::scale(model_transform, scaling);
            math::m3 model_transform_m3 = math::m3(model_transform);

            for(u32 i = 0; i < mesh->amt_verts; ++i)
            {
                math::v3 position = model_transform_m3 * mesh->vert_positions[i] + translation;
                aabb.min.x = std::min(position.x, aabb.min.x);
                aabb.min.y = std::min(position.y, aabb.min.y);
                aabb.min.z = std::min(position.z, aabb.min.z);
                    
                aabb.max.x = std::max(position.x, aabb.max.x);
                aabb.max.y = std::max(position.y, aabb.max.y);
                aabb.max.z = std::max(position.z, aabb.max.z);
            }

            return aabb;
        }

        RigidBody generateFromTriangleMesh(
            assets::triangle::TriangleMesh *mesh, 
            math::v3 pos, 
            math::quat quat,
            f32 inv_mass,
            math::v3 scaling, 
            math::v3 color,
            u32 id)
        {
            RigidBody rigid_body{};
            rigid_body.id = id;
            rigid_body.ref = mesh;

            math::m3 scaling_m3 = glm::scale(scaling); 

            updateQuat(&rigid_body, quat);

            rigid_body.color = math::v4(color, 1.);

            rigid_body.scaling = scaling;

            rigid_body.pos = pos;

            rigid_body.inv_mass = inv_mass;

            math::v3 com = calculateMeshCOM(mesh);
            rigid_body.zero_pos_to_com_offset = com;

            if(inv_mass != 0.)
            {
                math::m3 I = calculateMeshInertiaMatrix(mesh, com, 1. / inv_mass, scaling);
                math::m3 inv_I = glm::inverse(I);

                rigid_body.I = I;
                rigid_body.inv_I = inv_I;
            }
            else
            {
                rigid_body.inv_I = math::m3{0.};
            }

            return rigid_body;
        }


    }
}
