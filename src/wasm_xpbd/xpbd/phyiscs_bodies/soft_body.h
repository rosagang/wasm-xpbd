#pragma once

#include <algorithm> 

#include "../../maths/maths.h"

#include "../../assets/model_3d/teth_mesh.h"

#include "./body_id.h"

namespace RED
{
    namespace sim
    {
        f32 calc_teth_volume(math::v3 p1, math::v3 p2, math::v3 p3, math::v3 p4)
        {
            math::v3 p1p2 = p2 - p1;
            math::v3 p1p3 = p3 - p1;
            math::v3 p1p4 = p4 - p1;

            math::v3 cross = glm::cross(p1p2, p1p3);
            f32 vol = glm::dot(cross, p1p4) / 6.;

            return vol;
        }


        struct SoftBody
        {
            id::PhysBodyId id;
            //TODO move this somewhere else
            math::v3 color;

            assets::teth::TethMesh *ref;
            
            u32 amt_vertices;
            f32 *inv_masses;
            math::v3 *deformed_vertices;
            math::v3 *prev_positions_buffer;

            math::v3 *velocities;

            u32 amt_volume_constraints;
            f32 *rest_volumes;

            u32 amt_distance_constraints;
            f32 *rest_distances;

            f32 compliance;
        };

        SoftBody generateFromTethMesh(assets::teth::TethMesh *ref, f32 compliance, math::v3 translation, math::v3 color, id::PhysBodyId id)
        {
            SoftBody sb{};
            sb.id = id;
            
            sb.color = color;

            sb.compliance = compliance;

            sb.amt_vertices = ref->vertices.size();
            u32 vertices_size_in_bytes = ref->vertices.size() * sizeof(math::v3);

            sb.deformed_vertices = (math::v3 *) malloc(vertices_size_in_bytes);
            for(u32 i = 0; i < ref->vertices.size(); ++i)
            {
                sb.deformed_vertices[i] = ref->vertices[i] + translation;
            }

            sb.prev_positions_buffer = (math::v3 *) malloc(vertices_size_in_bytes);

            sb.velocities = (math::v3 *) malloc(vertices_size_in_bytes);
            memset(sb.velocities, 0, vertices_size_in_bytes);

            u32 inv_masses_size_bytes = sb.amt_vertices * sizeof(f32);
            sb.inv_masses = (f32 *) malloc(inv_masses_size_bytes);
            memset(sb.inv_masses, 0, inv_masses_size_bytes);

            sb.ref = ref;

            //calculate rest volumes and rest distances
            //first, distances

            sb.amt_distance_constraints = ref->teth_edge_indices.size() / 2;

            u32 dist_size_in_bytes = sb.amt_distance_constraints * sizeof(f32);
            sb.rest_distances = (f32 *) malloc(dist_size_in_bytes);

            for(u32 i = 0; i < ref->teth_edge_indices.size(); i += 2)
            {
                u32 i1 = ref->teth_edge_indices[i];
                u32 i2 = ref->teth_edge_indices[i + 1];
            
                math::v3 p1 = ref->vertices[i1];
                math::v3 p2 = ref->vertices[i2];
            
                f32 distance = glm::length(p1 - p2);
                sb.rest_distances[i / 2] = distance;            
            }

            //now, volumes and inv masses
            sb.amt_volume_constraints = ref->teth_indices.size() / 4;

            u32 volume_size_in_bytes = sb.amt_volume_constraints * sizeof(f32);
            sb.rest_volumes = (f32 *) malloc(volume_size_in_bytes);

            for(u32 i = 0; i < ref->teth_indices.size(); i += 4)
            {
                u32 i1 = ref->teth_indices[i];
                u32 i2 = ref->teth_indices[i + 1];
                u32 i3 = ref->teth_indices[i + 2];
                u32 i4 = ref->teth_indices[i + 3];

                math::v3 p1 = ref->vertices[i1];
                math::v3 p2 = ref->vertices[i2];
                math::v3 p3 = ref->vertices[i3];
                math::v3 p4 = ref->vertices[i4];

                f32 volume = calc_teth_volume(p1, p2, p3, p4);   
                assert(volume > 0.);            
                sb.rest_volumes[i / 4] = volume;

                f32 inv_mass = 1. / (volume / 4.);
                
                sb.inv_masses[i1] += inv_mass;
                sb.inv_masses[i2] += inv_mass;
                sb.inv_masses[i3] += inv_mass;
                sb.inv_masses[i4] += inv_mass;
            }

            return sb;
        }
    }
}
