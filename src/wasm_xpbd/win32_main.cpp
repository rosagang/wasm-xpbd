#include <stdio.h>

#include <typedefs.h>

#include "./assets/assets.h"

#include "./render/win32_opengl.h"
#include "./render/scene.h"

#include "./input/sdl_input.h"

#include "./xpbd/xpbd.h"
#include "./xpbd/grab_logic.h"
#include "./xpbd/sim_debug.h"

struct SDL_Timer
{
    u64 perf_freq_S;
    u32 start_ticks;
};

SDL_Timer timer;

u64 ticks_now()
{
    return SDL_GetPerformanceCounter();
}

f64 durationS(u64 start, u64 end)
{
    return ((f64) end - (f64) start) / ((f64) timer.perf_freq_S);
}

int main(int ArgCount, char **Args)
{
    f32 w = 1920;
    f32 h = 1080;
    SDL_Window *window = RED::render::createWindowAndInitOpenGL("xpbd", w, h);
    RED::input::SDL_Input sdl_input = RED::input::setup();    

    RED::assets::Assets assets;
    RED::assets::init(&assets);
    RED::render::ogl3::triMeshAdded(&assets.cube_mesh);
    RED::render::ogl3::triMeshAdded(&assets.plane_mesh);    

    timer.perf_freq_S = SDL_GetPerformanceFrequency();
    timer.start_ticks = SDL_GetPerformanceCounter();

    RED::render::cam::Camera cam;
    RED::render::cam::setupCamera(&cam, glm::vec3(0., 0., 16.), glm::vec3(0, 0, -1), w, h, glm::radians(45.));

    f64 last_frame_durS = 1. / 60.;

    b32 paused = true;

    RED::render::scene::Scene scene{};

    RED::sim::debug::SimDebug debug = RED::sim::debug::init(window, &scene, &cam);

    RED::sim::grab::Grab grab;
    RED::sim::Sim sim;
    RED::sim::init(&sim, &grab);      

    RED::math::v3 nice_red(.4, .1, .1);
    RED::math::v3 walls(.2, .1, .2);
    
    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(0., 0., 6), 
        glm::angleAxis(glm::radians(90.f), RED::math::v3(-1., 0., 0.)), 
        0, RED::math::v3(6, 6, 6), walls);

    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(0, -6, 0), 
        glm::angleAxis(glm::radians(0.f), RED::math::v3(0., 1., 0.)), 
        0, RED::math::v3(6, 6, 6), walls);

    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(0., 0., -6), 
        glm::angleAxis(glm::radians(90.f), RED::math::v3(1., 0., 0.)), 
        0, RED::math::v3(6, 6, 6), walls);

    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(6., 0., -0), 
        glm::angleAxis(glm::radians(90.f), RED::math::v3(0., 0., 1.)), 
        0, RED::math::v3(6, 6, 6), walls);

    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(-6., 0, -0), 
        glm::angleAxis(glm::radians(90.f), -RED::math::v3(0., 0., 1.)), 
        0, RED::math::v3(6, 6, 6), walls);

    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(0., 6, -0), 
        glm::angleAxis(glm::radians(180.f), -RED::math::v3(0., 0., 1.)), 
        0, RED::math::v3(6, 6, 6), walls);


    for(u32 i = 1; i < 101; ++i)    
    {
        float x = ((float)rand()/(float)(RAND_MAX)) * .5;
        float y = ((float)rand()/(float)(RAND_MAX)) * .5;
        float z = ((float)rand()/(float)(RAND_MAX)) * .5;
        RED::sim::RigidBody *rb = RED::sim::addRigidBodyFromTriangleMesh(
            &sim, &assets.cube_mesh, RED::math::v3(i * .01), 
            glm::angleAxis(glm::radians(150.f), RED::math::v3(1., 0., 0.)), 
            1.f, RED::math::v3(x + .2, y + .2, z + .2) , {x, y, z});
    }

    //sim.aabb_bucket_hash_grid.print();

    scene.point_light_1 = {0., 0, 6.};



    while (true)
    {
        last_frame_durS = std::min(last_frame_durS, 1. / 30.);
        u64 frame_beginning_ticks = ticks_now();
        RED::input::Input input = RED::input::generateFromSDL(&sdl_input);

        if(input.stop_running)
        {
            return 0;
        }

        RED::render::cam::move(&cam, RED::input::getMoveVec(input), last_frame_durS);
        RED::render::cam::rotate(&cam, input.rotCameraCmds.xy, last_frame_durS);
    
        //TODO reconsider rebuilding the scene object every frame
        scene.soft_bodies_to_render_ptr.clear();
        scene.rigid_bodies_to_render_ptr.clear(); 
        scene.rects.clear();       
        scene.lines.clear();


        for(u32 i = 1; i < sim.amt_rigid_bodies; ++i)
        {
            RED::sim::RigidBody *rigid_body = sim.rigid_bodies + i;
            RED::render::scene::pushRigidBody(&scene, rigid_body);
        }
    

        RED::sim::grab::handleGrabCommands(input, &grab, &sim, &cam, &scene);        

        if(input.step) paused = !paused;

        if(input.toggle_grav) sim.grav = !sim.grav;

        if(!paused || input.step_once)
        {   
            RED::math::v3 wall_turn_point = {0, 6, 0};
            RED::math::v3 wall_turn_axis = {0, 0, 1};
            for(u32 i = 0; i < 6; ++i)
            {
                RED::sim::RigidBody *wall = sim.rigid_bodies + i;
                RED::math::m4 trans = glm::translate(RED::math::m4(1), -wall->pos);
                trans = glm::rotate(trans, (f32) last_frame_durS * (f32) M_PI * 2 * .1f, wall_turn_axis);
                wall->pos = RED::math::m3(trans) * wall->pos;
                wall->quat = glm::toQuat(RED::math::m3(trans)) * wall->quat;
            } 
                          

            RED::sim::simOneFrame(&sim, last_frame_durS, &scene);
        }

        RED::render::renderSceneAndSwapWindows(window, &scene, &cam);   

        
        u64 frame_end_ticks = ticks_now();
        last_frame_durS = durationS(frame_beginning_ticks, frame_end_ticks);
        //printf("frame dur %f\n", last_frame_durS);
    }
    
    return 0;
}