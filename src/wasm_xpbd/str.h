#pragma once

#include <typedefs.h>

namespace RED
{
    namespace str
    {
        struct Str
        {
            const char* data;
            u32 size;

            ~Str()
            {
                delete[] data;
            }
        };
    }
}